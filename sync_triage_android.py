import os
import logging
import pymobilesupport.android
import extractors.android.googlephotos


def process_android_backup(path_to_backup_file):

    logging.info('Using Android Plugins:')
    logging.info('----------------------')
    list_of_devices = []

    android_backup_object = pymobilesupport.android.AndroidBackup(path_to_backup_file)

    logging.debug(android_backup_object.list_all())

    # Process Google Photos database
    identified_devices = extractors.android.googlephotos.process_google_photos_in_backup(android_backup_object)
    list_of_devices.extend(identified_devices)

    print('Found {} devices'.format(len(list_of_devices)))

    return list_of_devices
