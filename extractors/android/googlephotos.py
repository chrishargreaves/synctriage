import logging
import sqlite3
import os
import timecore
import core.synced_device

def process_google_photos_in_backup(android_backup_object):
    database_path = 'apps/com.google.android.apps.photos/db/gphotos0.db'
    database = android_backup_object.open_file(database_path)
    temp_filename = 'temp_gphoto.sqlite'
    out = open(temp_filename, 'wb')
    out.write(database.read())
    out.close()
    list_of_devices = process_google_photos_db(temp_filename, database_path)
    os.remove(temp_filename)
    return list_of_devices


def process_google_photos_db(path_to_db, orig_db_path):
    device_list = []
    conn = sqlite3.connect(path_to_db)
    c = conn.cursor()
    c.execute("""SELECT utc_timestamp, camera_make, camera_model, latitude, longitude, filename FROM remote_media""")
    results = c.fetchall()
    for i, each in enumerate(results):
        new_device = core.synced_device.SyncedDevice()
        if each[1]:
            new_device.make = each[1]
        if each[2]:
            new_device.model = each[2]
        timestamp = timecore.decode(each[0]/1000, 'unixtime', 'iso2')
        if timestamp:
            new_device.add_event(timestamp, '{},{}'.format(each[3], each[4]),
                                 'Photo taken ({})'.format(each[5]),
                                 '{}:remote_media:row_{}'.format(orig_db_path, i), utc=True)

        new_device.installed_software.append('Google Photos')
        if not new_device.isblank():
            device_list.append(new_device)

    return device_list


class fake_db():

    def __init__(self, file_pointer):
        self.file_pointer = file_pointer

