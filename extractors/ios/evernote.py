import logging
import extractors.generic.chrome

def get_devices_from_ios_evernote_db(ios_backup_object):
    """Recover devices from Chrome sync database if present"""
    logging.info('Processing Evernote Database...')

    # get user id fom AppDomainGroup-group.com.evernote.iPhone.Evernote\Library\Preferences\group.com.evernote.iPhone.Evernote.plist
    userid=''

    syncdb_id = ('AppDomainGroup-group.com.evernote.iPhone.Evernote', 'Documents\www.evernote.com\{}\Evernote7.sqlite'.format(userid))

    if ios_backup_object.exists(*syncdb_id):
        logging.info('Found Evernote database in backup. Processing...')
        file_path = ios_backup_object.get_abs_path(*syncdb_id)
        results = extractors.generic.chrome.get_results_from_db(file_path)
        return results
    else:
        logging.info('No Chrome Sync database found.')
        return []


def process_evernote_db_ios(path_to_db):
    pass
    # table is ZENNOTE
    # ZDATECREATED ZLATITUDE ZLONGITUDE ZTITLE, ZSOURCE ZSOURCEAPPLICATION