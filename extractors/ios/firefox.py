import logging
import sqlite3
import core.synced_device
import timecore
import datetime
import re

def process_firefox_browserdb_in_backup_folder(ios_backup_object):
    browserdb_id = ('AppDomainGroup-group.org.mozilla.ios.Firefox', 'profile.profile/browser.db')

    if ios_backup_object.exists(*browserdb_id):
        logging.info('Found Firefox browser.db in backup. Processing...')
        file_path = ios_backup_object.get_abs_path(*browserdb_id)
        results = process_firefox_browserdb(file_path)
        return results
    else:
        logging.info('No Firefox browser.db database found.')
        return []

def process_firefox_browserdb(path_to_browserdb):
    conn = sqlite3.connect(path_to_browserdb)
    c = conn.cursor()
    c.execute("""SELECT guid, name, modified, type, formfactor, os FROM clients""")
    results = c.fetchall()
    device_list = []
    for i, each in enumerate(results):
        new_device = core.synced_device.SyncedDevice()
        new_device.installed_software.append('Firefox')
        new_device.name = extract_device_name_from_name(each[1])
        new_device.info['device_type'] = each[3]
        new_device.info['formfactor'] = each[4]
        new_device.info['firefox_client_guid'] = each[0]

        username = extract_user_name_from_name(each[1])
        if username:
            new_device.info['username'] = username

        new_device.os = get_os_from_details(each[5])

        timestamp = timecore.decode(each[2]/1000, 'unixtime', out_format='iso2')
        if timestamp:
            new_device.add_event(timestamp[:19], '', 'Firefox client sync', path_to_browserdb + ':clients:row_{}'.format(i))

        # get open tabs for this client GUID
        open_tabs = get_open_tabs_for_user_as_events(path_to_browserdb, new_device.info['firefox_client_guid'])
        for each_event in open_tabs:
            new_device.add_event(*each_event)

        device_list.append(new_device)

    return device_list


def get_open_tabs_for_user_as_events(path_to_browserdb, username):
    """Scans tabs table for the supplied GUIDS and returns open tabs as events"""
    conn = sqlite3.connect(path_to_browserdb)
    c = conn.cursor()
    c.execute("""SELECT url, title, last_used FROM tabs WHERE client_guid LIKE ?""", (username,))
    events = []
    for i, each in enumerate(c.fetchall()):
        timestamp = timecore.decode(each[2]/1000, 'unixtime', 'iso2')
        evt = (timestamp, '', 'Firefox web visit {} ({})'.format(each[0], each[1]),
                                                       path_to_browserdb + ':tabs:row_{}'.format(i))
        events.append(evt)
    return events




def get_os_from_details(os_str):
    if os_str == 'WINNT':
        logging.debug('Converted WINNT to Windows for device.os')
        return 'Windows'
    else:
        return os_str


def extract_device_name_from_name(client_name):
    """Attemtpt to extract device name from client name"""
    result = re.search('.* on (.*)', client_name)
    if result:
        device_name = result.group(1)
        return device_name
    else:
        return '[Unknown]'

def extract_user_name_from_name(client_name):
    """Attemtpt to extract username from client name"""
    result = re.search('(.*)\'s Firefox on .*', client_name)
    if result:
        user_name = result.group(1)
        return user_name
    else:
        return None

