import logging
import sqlite3
import re
import os
import timecore
import core.synced_device

def process_sms_in_backup_folder(ios_backup_object):

    sms_id = ('HomeDomain', 'Library/SMS/sms.db')

    if ios_backup_object.exists(*sms_id):
        logging.info('Found sms.db in backup. Processing...')
        file_path = ios_backup_object.get_abs_path(*sms_id)
        results = process_sms_db(file_path, '{}:{}'.format(sms_id[0], sms_id[1]))
        return results
    else:
        logging.info('No SMS databsae found.')
        return []


def process_sms_db(path_to_sms_db, orig_path):
    conn = sqlite3.connect(path_to_sms_db)
    c = conn.cursor()
    c.execute("""SELECT text,date FROM message""")
    results = c.fetchall()
    device_list = []

    for i, each in enumerate(results):
        service_name = detect_known_services(each[0])
        if service_name:
            new_device = core.synced_device.SyncedDevice()
            new_device.installed_software.append(service_name)
            timestamp = timecore.decode(each[1], 'macabs', out_format='iso2')
            source_path = '{}:message:row_{} ({})'.format(os.path.basename(path_to_sms_db), i, orig_path)
            new_device.add_event(timestamp, '', '{} registered on device'.format(service_name), source_path)
            device_list.append(new_device)
    return device_list


def detect_known_services(message_text):
    """Searches the message test for known service sign-in messages"""
    if message_text is None or not isinstance(message_text, str):
        logging.warning('SMS message {} not text ({})'.format(str(message_text), type(message_text)))
        return None

    if re.search('Telegram code', message_text):
        logging.info('Detected Telegram sign-in on unknown device')
        return 'Telegram'
    if re.search('Your Viber code is', message_text):
        logging.info('Detected Viber sign-in on unknown device')
        return 'Viber'
    if re.search('to verify your Instagram account', message_text):
        logging.info('Detected Instagram sign-in on unknown device')
        return 'Instagram'
    if re.search('Your Twitter confirmation code is ', message_text):
        logging.info('Detected Twitter sign-in on unknown device')
        return 'Twitter'
    if re.search('Your Google verification code is ', message_text):
        logging.info('Detected Google sign-in on unknown device')
        return 'Google'
    if re.search('Your WhatsApp code is ', message_text):
        logging.info('Detected WhatsApp sign-in on unknown device')
        return 'WhatsApp'
    # TODO there are more that can be added here

    logging.debug('no sign-in text identified in this message: \'{}\''.format(message_text))
    return None