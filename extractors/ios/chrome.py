import logging
import extractors.generic.chrome

def get_devices_from_ios_chrome_syncdb(ios_backup_object):
    """Recover devices from Chrome sync database if present"""
    logging.info('Processing Chrome Sync Database...')
    syncdb_id = ('AppDomain-com.google.chrome.ios', 'Library/Application Support/Google/Chrome/Default/Sync Data/SyncData.sqlite3')

    if ios_backup_object.exists(*syncdb_id):
        logging.info('Found Chrome SyncData database in backup. Processing...')
        file_path = ios_backup_object.get_abs_path(*syncdb_id)
        results = extractors.generic.chrome.get_results_from_db(file_path)
        return results
    else:
        logging.info('No Chrome Sync database found.')
        return []
