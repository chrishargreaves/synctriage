import core.synced_device
import thirdparty.cclbplist.ccl_bplist


class SafariSyncExtractor(object):

    def __init__(self, path_to_safari_data):
        self.devices = self.__process_safari_plist(path_to_safari_data)

    def get_detected_devices(self):
        """Returns a list of the names of the detected devices"""
        return self.devices


    def __process_safari_plist(self, path_to_safari_data):
        """Process Safari database and populate info about other devices"""
        file_pointer = open(path_to_safari_data, 'rb')
        file_pointer.seek(0)
        plist_parser_data = thirdparty.cclbplist.ccl_bplist.load(file_pointer)

        values = plist_parser_data['values']
        print('{} devices detected'.format(len(values)))

        device_list = []

        for each in values:
            dev_name = values[each]['value']['DeviceName']

            the_device = core.device.SyncedDevice()
            the_device.name = dev_name
            the_device.report += 'Safari Tabs Open:\n'
            the_device.report += '=================\n'

            if 'Tabs' in values[each]['value']:
                tabs = values[each]['value']['Tabs']
                for each_tab in tabs:
                    the_device.report += '{} ({})\n'.format(each_tab['Title'], each_tab['URL'])

            device_list.append(the_device)

        return device_list
