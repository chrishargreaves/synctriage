import os
import logging
import sqlite3
import datetime
import core.synced_device

def get_devices_from_evernote(path_to_home_folder):
    """Given a user profile, locates an Evernote database and returns devices from it"""
    database_name = get_database_name(path_to_home_folder)
    devices = []
    if database_name:
        logging.info('Detected Evernote database ({}) from {}'.format(database_name, path_to_home_folder))
        devices.extend(get_devices_from_database(database_name))
    return devices


def get_database_name(path_to_home_folder):
    """Given a Windows user home folder, locates an Evernote database from a .accounts file if present"""
    path_to_evernote_databases = os.path.join(path_to_home_folder, 'AppData', 'Local', 'Evernote',
                                         'Evernote', 'Databases')
    path_to_accounts_file = os.path.join(path_to_evernote_databases, '.accounts')
    if os.path.exists(path_to_accounts_file):
        f = open(path_to_accounts_file, 'rb')
        data = f.read()
        parts = data.split(b';')
        user_account = parts[0].decode()
        return os.path.join(path_to_evernote_databases, user_account + '.exb')
    else:
        return None


def get_devices_from_database(path_to_db):
    """Extracts and returns devices identified from the specificed everntoe data"""
    list_of_devices = []
    conn = sqlite3.connect(path_to_db)
    c = conn.cursor()
    c.execute("""SELECT title, date_created, date_updated, source, source_app, latitude, longitude, geo_address, author
FROM note_attr""")
    results = c.fetchall()

    for i, (db_title, db_date_created, db_date_updated, db_source, db_source_app, db_lat, db_long, db_geo, db_author) in enumerate(results):
        poss_os = get_os_from_db_details(db_source, db_source_app)
        if poss_os:
            existing_device = get_index_of_device_in_list(poss_os, list_of_devices)
            if existing_device is not None:
                logging.debug('Already got a {} device - modifying that one'.format(poss_os))
                new_device = list_of_devices.pop(existing_device) # remove temporarity so can be updated
            else:
                logging.debug('Not seen a {} device before - making new'.format(poss_os))
                new_device = core.synced_device.SyncedDevice()
                new_device.source_file = path_to_db
                new_device.os = poss_os
                new_device.installed_software.append('Evernote')
                if db_author:
                    new_device.info['Evernote User'] = db_author


            if db_lat == None or db_long == None:
                location = ''
            else:
                location = '{}, {}'.format(round(db_lat,5), round(db_long,5))

            # need to add events too
            if db_date_created is not None:
                new_device.add_event(ordinalToGregorian(db_date_created), location, 'EverNote note created', '{}:note_attr:{}'.format(path_to_db,i), utc=True)
            if db_date_created != db_date_updated and db_date_updated is not None:
                new_device.add_event(ordinalToGregorian(db_date_updated), '', 'EverNote note updated', '{}:note_attr:{}'.format(path_to_db,i), utc=True)


            list_of_devices.append(new_device)

    return list_of_devices


def ordinalToGregorian(o):
    # from forensic focus - topic 6573676
    l = int(o)
    d = datetime.datetime.fromordinal(l)     # use the int component to get date
    t = datetime.timedelta(seconds=((24 * 60 * 60) * (o-l)))   # use the seconds component to get a time
    return str(d + t)[0:19]


def get_index_of_device_in_list(the_os, the_list):
    for i, each in enumerate(the_list):
        if each.os == the_os:
            return i
    return None


def get_os_from_db_details(source, source_app):
    if source == 'mobile.iphone':
        return 'iOS'
    if source == 'mobile.ios':      # usually this will have an app name too e.g. penultimate. Not implemented yet.
        return 'iOS'
    if source == 'desktop.win':
        return 'Windows'
    if source == 'desktop.mac':
        return 'Mac OS'
    if source == 'mobile.android':
        return 'Android'

    # Unknown ones
    if source == 'mail.smtp':
        return None
    if source == 'web.clip':
        return None

    logging.warning('Failed to detect OS from {},{}'.format(source, source_app))

    return None
