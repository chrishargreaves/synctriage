import os
import sqlite3
import timecore
import core.synced_device

def get_devices_from_viber(path_to_home_folder):
    """Returns a list of devices from a Viber app install"""
    path_to_config_db = os.path.join(path_to_home_folder, 'AppData', 'Roaming', 'ViberPC', 'config.db')
    if not os.path.exists(path_to_config_db):
        return []
    list_of_devices = get_device_from_db(path_to_config_db)
    return list_of_devices


def get_device_from_db(path_to_config_db):
    """Builds a device from the accoutn info in Viber config.db"""
    device_list = []
    conn = sqlite3.connect(path_to_config_db)
    c = conn.cursor()
    c.execute("""SELECT ID, Email, NickName, TimeStamp FROM Accounts""")
    results = c.fetchall()
    for i, (viber_id, viber_email, viber_name, viber_timestamp) in enumerate(results):

        # timestamp might be account sign up
        #print(timecore.decode(viber_timestamp, 'unixtime', 'iso2'))

        device = core.synced_device.SyncedDevice()
        device.installed_software.append('Viber')
        device.info['phone number'] = viber_id
        if viber_name:
            device.info['viber username'] = viber_name
        if viber_email:
            device.info['viber email'] = viber_email
        device_list.append(device)
    return device_list
