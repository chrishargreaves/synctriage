import os
import logging
import extractors.generic.exif_tool

def get_devices_from_dropbox_paths(root_to_home_folder):
    logging.info('Processing Dropbox Photos...')
    device_list = []

    downloads_path = os.path.join(root_to_home_folder, 'Dropbox', 'Camera Uploads')
    logging.debug('Scanning {}'.format(downloads_path))
    res = extractors.generic.exif_tool.scan_folder_of_images(downloads_path)
    device_list.extend(res)

    logging.info('Identified {} devices from Dropbox Camera Upload photos'.format(len(device_list)))
    return device_list
