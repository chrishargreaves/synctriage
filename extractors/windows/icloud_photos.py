import os
import re
import logging
import timecore
import sqlite3
import extractors.generic.exif_tool
import core.synced_device

def get_devices_from_icloud_paths(root_to_home_folder):
    logging.info('Processing iCloud Photos...')
    device_list = []
    downloads_path = os.path.join(root_to_home_folder, 'Pictures', 'iCloud Photos', 'Downloads')
    logging.debug('Scanning {}'.format(downloads_path))
    res = extractors.generic.exif_tool.scan_folder_of_images(downloads_path)
    device_list.extend(res)

    sharing_path = os.path.join(root_to_home_folder, 'Pictures', 'iCloud Photos', 'Shared')
    logging.debug('Scanning {}'.format(sharing_path))
    res = extractors.generic.exif_tool.scan_folder_of_images(sharing_path)
    device_list.extend(res)

    logging.info('Identified {} devices from iCloud Photos'.format(len(device_list)))
    return device_list


def get_device_info_from_databases(root_to_home_folder):
    path_to_db = os.path.join(root_to_home_folder, 'AppData', 'Local', 'Apple Inc', 'iCloudPhotoLibrary', 'client.db')
    results = get_result_from_icloud_clientdb(path_to_db)
    return results

def get_result_from_icloud_clientdb(path_to_db):
    list_of_devices = []
    if not os.path.exists(path_to_db):
        logging.warning('iCloud database not found at {}'.format(path_to_db))
        return list_of_devices

    conn = sqlite3.connect(path_to_db)
    c = conn.cursor()
    #c.execute("""SELECT * FROM server_items""")
    c.execute("""SELECT filename, creation_date FROM server_items""")
    results = c.fetchall()
    ios_device = core.synced_device.SyncedDevice()

    # so this makes a single iOs device form all the photos in the database
    # is it possible to distinguish multiple devices?
    # might be resolution in asset_rec and master_rec

    for i, (filename, creation_date) in enumerate(results):
        if re.match('IMG_.+\.JPG', filename):
            logging.info('Likely iOS device photograph detected')
            the_date = timecore.decode(int(creation_date), 'macabs', out_format='iso2')
            source_file = path_to_db + ':server_items'
            source_row = source_file + ':row_{}'.format(i)
            ios_device.add_event(the_date, '', 'Photograph taken', source_row, utc=True)
            # there are 3 other dates in database
            # import_date, asset_date, added_date, these could also be added as events

            if len(ios_device.events) > 0:
                # did get something of use
                # add the rest of the info
                ios_device.source_file = source_file
                ios_device.os = '[iOS]'
                ios_device.make = '[Apple]'

    return [ios_device,]





