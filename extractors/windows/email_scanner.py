import os
import logging
import extractors.generic.email_scanner

def get_devices_from_windows_emails(path_to_user_home):
    """Checks for other devices in emails in Windows Unistore path"""
    list_of_emails = get_emails_from_unistore(path_to_user_home)
    devices = []
    for each_file in list_of_emails:
        result = extractors.generic.email_scanner.scan_email_file_for_signup(each_file)
        if result:
            devices.append(result)
    return devices

def get_emails_from_unistore(path_to_user_home):
    """Returns a list of files in the unistore path"""
    email_path = os.path.join(path_to_user_home, 'AppData', 'Local', 'Comms', 'Unistore', 'data')
    email_file_list = get_emails_from_folder(email_path)
    return email_file_list

def get_emails_from_folder(target_folder):
    """Returns a list of file from the target_folder"""
    logging.info('Scanning {} for emails'.format(target_folder))
    email_list = []
    for root, dirs, files in os.walk(target_folder, topdown=False):
        for name in files:
            email_list.append(os.path.join(root, name))
    logging.info('Found {} emails'.format(len(email_list)))
    return email_list
