import os
import re
import sqlite3
import logging
import core.synced_device


def get_devices_from_session_sync_db(root_of_home_folder):
    """Recover devices from Chrome sync database if present"""
    logging.info('Processing Chrome Sync Database...')
    chrome_user_data_path = os.path.join(root_of_home_folder, 'AppData', 'Local', 'Google', 'Chrome', 'User Data')

    logging.warning('Not scanned for multiple Chrome users')
    logging.info('Processing Default Chrome user')
    syncdb_path = os.path.join(chrome_user_data_path, 'Default', 'Sync Data', 'SyncData.sqlite3')
    logging.debug('Scanning {}'.format(syncdb_path))
    try:
        results = get_results_from_db(syncdb_path)
    except sqlite3.OperationalError:
        results = []
        print('ERROR: CHROME DATABASE LOCKED. CANNOT ACCESS. ')
    logging.info('{} devices detected in database'.format(len(results)))
    return results


def get_list_of_chrome_users(chrome_folder_path):
    pass

# def get_results_from_db(syncdb_path):
#     list_of_devices = []
#     conn = sqlite3.connect(syncdb_path)
#     c = conn.cursor()
#     c.execute("""SELECT non_unique_name, specifics FROM metas""")
#     results = c.fetchall()
#     for each in results:
#         if each[0] and each[1]: # they can be None in some cases
#             new_device = core.synced_device.SyncedDevice()
#             new_device.source_file = syncdb_path
#             if re.search(b"session_sync", each[1]):
#                 new_device.name = each[0]
#                 list_of_devices.append(new_device)
#     return list_of_devices


def get_results_from_db(syncdb_path):
    list_of_devices = []
    if not os.path.exists(syncdb_path):
        logging.warning('Chrome Sync databse not found at {}'.format(syncdb_path))
        return list_of_devices

    conn = sqlite3.connect(syncdb_path)
    c = conn.cursor()
    c.execute("""SELECT non_unique_name, specifics FROM metas""")
    results = c.fetchall()
    for each in results:
        if each[0] and each[1]: # they can be None in some cases
            new_device = core.synced_device.SyncedDevice()
            new_device.source_file = syncdb_path
            if re.search(b"branch-heads", each[1]): # if specifics contain 'branch-heads'
                #if re.match(b"\xd2\xb9", each[1]): # https://github.com/marleyjaffe/ChromeSyncParser/blob/master/ChromeParser.py
                new_device.name = each[0]
                software_version = extract_software_version(each[1])
                if software_version:
                    new_device.installed_software.append(software_version)
                if re.search(b'Chrome WIN', each[1]):
                    new_device.os = 'Windows'
                    new_device.log('Device is running Windows (based on Chrome Sync Artefacts)')
                if re.search(b'Chrome MAC', each[1]):
                    new_device.os = 'Mac OS'
                    new_device.log('Device is running Mac OS (based on Chrome Sync Artefacts)')
                if re.search(b'Chrome ANDROID-PHONE', each[1]):
                    new_device.os = 'Android'
                    new_device.log('Device is running Android (based on Chrome Sync Artefacts)')
                if re.search(b'Chrome IOS-PHONE', each[1]):
                    new_device.os = 'iOS'
                    new_device.log('Device is running iOS (based on Chrome Sync Artefacts)')

                poss_urls = get_refs_to_device(new_device.name, syncdb_path)
                for each_url in poss_urls:
                    new_device.add_info('chrome url visit', each_url)

                list_of_devices.append(new_device)
    return list_of_devices

def get_refs_to_device(device_name, syncdb_path):
    conn = sqlite3.connect(syncdb_path)
    c = conn.cursor()
    c.execute("""SELECT specifics FROM metas WHERE non_unique_name LIKE ?""", (device_name,))
    results = c.fetchall()
    urls = []
    for each in results:
        res = re.search(b'(http.*?)[\x1a|\xa0|\\|\']', each[0])
        if res:
            try:
                urls.append(res.group(1).decode('utf-8'))
            except UnicodeDecodeError:
                urls.append(str(res.group(1)))
                print('error decoding {}'.format(str(res.group(1))))
    return urls



def dump_db(db_path):
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("""SELECT * FROM metas""")
    results = c.fetchall()
    for each in results:
        print(each)

def extract_software_version(data):
    result = re.search(b'(Chrome.*) \(', data)
    if result:
        return result.group(1).decode()
    else:
        return None

