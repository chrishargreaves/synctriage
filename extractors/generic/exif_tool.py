import os
import logging
import exifread
import core.synced_device


def scan_folder_of_images(path_to_folder):
    """Scans a folder for image files and detects any devices."""
    if not os.path.exists(path_to_folder):
        logging.warning('Path {} does not exist'.format(path_to_folder))
        return []

    device_list = []
    list_of_files = os.listdir(path_to_folder)
    logging.debug('Found {} files'.format(len(list_of_files)))
    for each_file in list_of_files:
        full_path = os.path.join(path_to_folder, each_file)
        # open with exif tool
        dev = get_device_from_image_exif_data(full_path)
        if dev:
            device_list.append(dev)

    return device_list


def get_device_from_image_exif_data(path_to_image):
    """Extracts a device and associated info from an image file's EXIF data"""
    if not os.path.isfile(path_to_image) or not os.path.exists(path_to_image):
        return

    f = open(path_to_image, 'rb')
    # check image has tags/is an image

    if not check_is_image(path_to_image):
        logging.debug('File {} is not an image, skipping.'.format(path_to_image))
        return None

    tags = exifread.process_file(f)
    new_device = core.synced_device.SyncedDevice()
    new_device.source_file = path_to_image

    exif_make = tags.get('Image Make')
    exif_model = tags.get('Image Model')
    exif_software = tags.get('Image Software')

    # GPS TODO
    lat = tags.get('GPS GPSLatitude')
    lat_ref = tags.get('GPS GPSLatitudeRef')
    long = tags.get('GPS GPSLongitude')
    long_ref = tags.get('GPS GPSLongitudeRef')
    gps_date = tags.get('GPS GPSDate')
    gps_time = tags.get('GPS GPSTimeStamp')

    if lat and lat_ref and long and long_ref:
        lat_str = '{}'.format(gps_to_str(str(lat)))
        long_str = '{}'.format(gps_to_str(str(long)))
        location = '{}{}, {}{}'.format( lat_str, lat_ref, long_str, long_ref)
    else:
        location = ''

    gps__timestamp = gps_date_time_as_string(gps_date, gps_time)
    if gps__timestamp:
        #print(gps__timestamp)
        pass

    orig_time = tags.get('EXIF DateTimeOriginal')   # note that the exif data is local time! need to figur eout a way to fix that
    if orig_time is not None and orig_time != 'None':
        new_device.add_event(str(orig_time) + '', location, 'Picture was taken', path_to_image)

    image_time = tags.get('Image DateTime')
    if image_time is not None and image_time != 'None' and str(image_time) != str(orig_time):
        new_device.add_event(str(image_time) + '', location, 'Picture was taken', path_to_image)

    if not exif_make and not exif_model:
        # new_device.make = None
        # new_device.model = None
        pass
    else:
        new_device.make = str(exif_make)
        new_device.model = str(exif_model)
        new_device.log('Added make and model from EXIF data')

    if exif_software:
        new_device.info['EXIF Image Software'] = str(exif_software)
    new_device.log('Added EXIF Image Software')

    if new_device.isblank():
        logging.warning('No EXIF information found in {}. Creation software was {}'.format(path_to_image, exif_software))
        return None
    else:
        return new_device


def check_is_image(file_path):
    f = open(file_path, 'rb')
    data = f.read(512)
    if data[0:3] == b'\xff\xd8\xff': # jpg
        return True
    elif data[0:4] == b'\x89\x50\x4E\x47': # png
        return True
    else:
        return False


def gps_date_time_as_string(the_date, the_time):
    if the_date is None or the_time is None or the_date == 'None' or the_time == 'None':
        return None

    date_str = str(the_date)
    time_str = str(the_time)

    date_component = date_str.split(':')
    if len(date_component) != 3:
        logging.error('Error decoding GPS date {}'.format(str(the_date)))
        return None
    out_str = '{}-{}-{}'.format(*date_component)

    if time_str[0] == '[':
        time_component = eval(time_str)
        out_str += ' {}:{}:{}'.format(*time_component)
        return out_str[0:19]
    else:
        logging.error('Error decoding GPS time {}'.format(str(the_time)))
        return None


def infer_timezone(utc, local):
    pass
    # print(utc)
    # print(local)


def gps_to_str(in_str):
    if in_str[0] == '[':
        res = eval(in_str)
    else:
        return None
    value = res[0] + (res[1] / 60.0) + (res[2] / 3600.0)
    out = '{}'.format(round(value,5))
    return out
