import re
import logging
import core.synced_device
import core.device_entity_finder
import html.parser
import os

# scan folder of emails to identify signups on devices

def scan_email_file_for_signup(path_to_email_file):
    """Scans the specified file for any known signups"""
    if not os.path.exists(path_to_email_file):
        logging.error('File does not exist: {}'.format(path_to_email_file))
        return

    f = open(path_to_email_file, 'rb')
    data = f.read()
    if is_html(data):
        logging.debug('Scanning email {} for signups'.format(path_to_email_file))
        result = scan_email_for_signup(data)
        if result:
            result.source_file = path_to_email_file
        return result
    else:
        # not html, ignoring
        pass


def scan_email_for_signup(email_file_content):
    """Scans the email_file for any known signup, registration or device used info"""
    if is_html(email_file_content):
        res = check_for_chrome(email_file_content)
        if res:
            logging.info('Found Chrome reference in email')
            return res
        res = check_for_icloud(email_file_content)
        if res:
            logging.info('Found iCloud reference in email')
            return res
        res = check_for_firefox(email_file_content)
        if res:
            logging.info('Found Firefox reference in email')
            return res
        res = check_for_dropbox1(email_file_content)
        if res:
            logging.info('Found Dropbox reference in email')
            return res
        res = check_for_dropbox2(email_file_content)
        if res:
            logging.info('Found Dropbox reference in email')
            return res
        res = check_for_google(email_file_content)
        if res:
            logging.info('Found Google reference in email')
            return res
    else:
        # not html - nothing to scan
        pass



# TODO move to generic codebase
def is_html(content):
    """Checks that the content is """
    if content[0:5].lstrip() == b'<html':
        return True
    if content[0:14].lstrip() == b'<!DOCTYPE html':
        return True
    else:
        return False


def check_for_google(email_content):
    """Checks if email contains Google notification of use on other devices"""
    device = core.synced_device.SyncedDevice()
    res = re.search(b'Your Google Account (.+) was just used to sign in (?:on|from) (.+?)\.', email_content)
    if res:
        device.info['Google Account'] = res.group(1).decode()
        device_description = res.group(2).decode()
        device_description = strip_tags(device_description)

        device.ingest_data_from_string(device_description)

        return device
    else:
        return None


def check_for_chrome(email_content):
    """Checks if email contains Chrome notification of use on other devices"""
    device = core.synced_device.SyncedDevice()
    res = re.search(b'New sign-in from Chrome on ([0-9a-zA-Z]+)', email_content)
    if res:
        device.installed_software.append('Chrome')
        device_description = res.group(1).decode()
        device.ingest_data_from_string(device_description)
        return device
    else:
        return None


def check_for_dropbox1(email_content):
    """Checks if email contains Dropbox notification of use on other devices"""
    device = core.synced_device.SyncedDevice()
    res = re.search(b"You've connected an? ([0-9a-zA-Z]+) to Dropbox", email_content)
    if res:
        device.installed_software.append('Dropbox')
        device_description = res.group(1).decode()
        device.ingest_data_from_string(device_description)
        return device
    else:
        return None

def check_for_dropbox2(email_content):
    """Checks if email contains Dropbox notification of use on other devices"""
    device = core.synced_device.SyncedDevice()
    res = re.search(b"We see that you've linked a new computer, '(.+)', to your Dropbox",
                    email_content)
    if res:
        device.installed_software.append('Dropbox')
        device_description = res.group(1).decode()
        device.name = device_description
        device.ingest_data_from_string(device_description)

        return device
    else:
        return None

def check_for_firefox(email_content):
    """Checks if email contains Firefox notification of use on other devices"""
    device = core.synced_device.SyncedDevice()

    # check that it is a firefox email
    firefox_check = re.search(b'New sign-in to Firefox', email_content)
    if firefox_check:
        res = re.search(b'(Firefox [a-zA-Z0-9 ]+), (.+)<br/>(.+)</p>', email_content)
        if res:
            device_description = res.group(0).decode()
            install_time = res.group(3).decode()
            device.installed_software.append(res.group(1).decode())
            device.ingest_data_from_string(device_description)

            # need to add event properly
            #print('event: firefox installation at {}'.format(install_time))
        else:
            device.installed_software.append('Firefox')
        return device
    else:
        return None

def check_for_icloud(email_content):
    """Checks if email contains Chrome notification of use on other devices"""
    device = core.synced_device.SyncedDevice()
    res = re.search(b'Your Apple ID \((.+)\) was used to sign in to (.+) on (.+).', email_content)
    if res:
        device.info['iCloud Account'] = strip_tags(res.group(1).decode())

        poss_sw_match = res.group(2).decode()
        poss_device_match = res.group(3).decode()

        device.ingest_data_from_string(poss_sw_match)
        device.ingest_data_from_string(poss_device_match)

        # look for device OS in the whole content
        the_os = core.device_entity_finder.find_os(email_content.decode())
        if the_os:
            device.os = the_os

        the_name = re.search(b'named \xE2\x80\x9C(.*)\xE2\x80\x9D', res.group()) # device name is in quotes
        if the_name:
            device.name = the_name.group(1).decode()

        return device
    else:
        return None







# TODO This needs moving to generic code

class MLStripper(html.parser.HTMLParser):
    # http://stackoverflow.com/questions/11061058/using-htmlparser-in-python-3-2
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()
