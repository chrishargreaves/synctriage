import logging
import operator
import core.device_entity_finder
import core.merged_synced_device_list

def preprocess_device_list(list_of_devices):
    """Pre-process device list and auto-infer OS, make where possible"""
    print()
    print('Pre-processing to infer OS etc...')
    logging.info('-' * 25)
    logging.info('Pre-processing to infer OS etc...')
    preprocess_updates = 0
    for each in sorted(list_of_devices, key=operator.attrgetter('no_name', 'name_lower', 'make', 'model', 'os')):
        if each.os == '[Unknown]':
            inferred_os = core.device_entity_finder.infer_os_from_model(each.model)  # try from model
            if inferred_os:
                logging.info('Inferred os \'{}\' from \'{}\' in {}'.format(inferred_os, each.model, each.name))
                each.os = inferred_os
                preprocess_updates += 1
        if each.make == '[Unknown]':
            inferred_make = core.device_entity_finder.infer_make_from_model(each.model)
            if inferred_make:
                logging.info('Inferred make \'{}\' from \'{}\' in {}'.format(inferred_make, each.model, each.name))
                each.make = inferred_make
                preprocess_updates += 1
        if each.make == '[Unknown]':
            inferred_make = core.device_entity_finder.infer_make_from_os(each.os)
            if inferred_make:
                logging.info('Inferred make \'{}\' from \'{}\' in {}'.format(inferred_make, each.os, each.name))
                each.make = inferred_make
                preprocess_updates += 1

    print('Made {} updates using pre-processor'.format(preprocess_updates))
    logging.info('Made {} updates using pre-processor'.format(preprocess_updates))
    logging.info('-'*20)

    return list_of_devices


def merge_devices(list_of_devices):
    logging.info('-' * 25)
    logging.info('Merging devices...')
    print()
    print('Merging {} devices... '.format(len(list_of_devices)), end='')

    merged_device_list = core.merged_synced_device_list.MergedDeviceList()
    merged_device_list.build_from_list(
        sorted(list_of_devices, key=operator.attrgetter('name_lower', 'make', 'model', 'os')))

    print('now {} total devices'.format(len(merged_device_list)))
    print()
    logging.info('Now {} total devices'.format(len(merged_device_list)))
    logging.info('=' * 20)
    return merged_device_list

