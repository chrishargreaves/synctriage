import os
import re
import logging
import dfpy.utils.os_detection
import pymobilesupport.iphone_backup

def find_relevant_files(target_path, device_name_list):

    logging.info('Entered research mode...')
    logging.info('Seaching for files that contain the known device names...')
    logging.debug(str(device_name_list))

    target = dfpy.utils.os_detection.detect_os_in_target_path(target_path)
    if target == 'ios_backup':
        ios_backup_obj = pymobilesupport.iphone_backup.iOSBackup(target_path)
    else:
        logging.warning("Research mode supports iOS target only at present. Other platforms will return no results.")

    results = []
    for root, dirs, files in os.walk(target_path, topdown=False):
        for name in files:
            full_path = os.path.join(root, name)
            file_results = search_file_for_device_names(full_path, device_name_list)
            if target == 'ios_backup':
                new_list = []
                for each_result in file_results:
                    hash_file = os.path.basename(each_result[1])
                    a = ios_backup_obj.lookup.get(hash_file)
                    new_list.append((each_result[0], each_result[1], a))
                file_results = new_list
                #file_results = [x.append(ios_backup_obj.lookup.get(os.path.basename(x[1]))) for x in file_results]
            results.extend(file_results)
    return results

def search_file_for_device_names(filename, device_name_list):
    results = []
    if not os.path.exists(filename):
        return []

    if os.stat(filename).st_size > 300 * 1024 * 1024:
        logging.warning('Skipped searching a large file ({})'.format(filename))
        return []

    try:
        f = open(filename, 'rb')
        data = f.read()
        for each_device_name in device_name_list:
            utf8_res = re.search(each_device_name.encode('utf8'), data)
            utf16_res = re.search(each_device_name.encode('utf16'), data)
            if utf8_res or utf16_res:
                logging.info('Hit for {} in {}'.format(each_device_name, filename))
                results.append([each_device_name, filename])
    except PermissionError:
        logging.error('Permission denied accessing {}'.format(filename))
        return []

    return results


