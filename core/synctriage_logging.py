import logging
import os


def enable_logging(verbose, log_path=None):
    """Enables logging to the path specified or working directory if None is specified.
    Returns None if logging could not be started"""
    log_file_name = 'log.txt'

    if log_path:
        if os.path.exists(log_path):
            full_log_path = os.path.join(log_path, log_file_name)
        else:
            return None
    else:
        full_log_path = log_file_name

    if verbose:
        logging.basicConfig(handlers=[logging.FileHandler(full_log_path, 'w', 'utf-8')],
                            format='%(asctime)s %(levelname)-8s %(message)s \t\t[%(module)s.%(funcName)s()]',
                            level=logging.DEBUG)
    else:
        logging.basicConfig(handlers=[logging.FileHandler(full_log_path, 'w', 'utf-8')],
                            format='%(asctime)s %(levelname)-8s %(message)s',
                            level=logging.INFO)

    logging.info("SyncTriage started")
    return True

