import re

def find_os(content):
    # these get more specific, the last match will be returned
    # this is so we scan for everything, can log hits and use for debugging

    opearating_systems = ['Windows',
                          'Windows XP',
                          'Windows Vista',
                          'Windows 7',
                          'Windows 8',
                          'Windows 10',
                          'Mac',
                          'Mac OS',
                          'Mac OS X',
                          'Ubuntu',
                          'iOS',
                          'iOS 8',
                          'iOS 9',
                          'iOS 9.1',
                          'iOS 9.2',
                          'iOS 9.3',
                          'Android']
    return_value = None
    for each in opearating_systems:
        res = re.search(each.lower(), content.lower())
        if res:
            return_value = each

    # returns the last value found
    return return_value

def get_major_os_from_string(os_string):
    opearating_systems = ['Windows',
                          'Mac OS',
                          'Ubuntu',
                          'iOS',
                          'Android']
    for each_major_os in opearating_systems:
        res = re.search(each_major_os.lower(), os_string.lower())
        if res:
            return each_major_os


def find_device(content):
    # these get more specific, the last match will be returned
    # this is so we scan for everything, can log hits and use for debugging

    device_types = ['iPhone',
                    'iPhone 4',
                    'iPhone 4S',
                    'iPhone 5',
                    'iPhone 5S',
                    'iPhone 6',
                    'iPhone 6S',
                    'iPhone 7',
                    'iPad',
                    'iPad Mini'
                    'iPad Air'
                    'iPad Pro'
                    'iPod',
                    'iPod Touch'
                    'Nexus',
                    'Nexus 4',
                    'Nexus 5',
                    'Nexus 6',
                    'Nexus 7',
                    'Nexus 8',
                    'Nexus 9',
                    'Nexus 10',
                    # more to do here...
                    ]
    return_value = None
    for each in device_types:
        res = re.search(each.lower(), content.lower())
        if res:
            return_value = each

    # returns the last value found
    return return_value

def infer_os_from_model(device):
    """Tries to map device models to OS as best it can"""
    mapping = { 'Nexus': '[Android]',
                'Galaxy': '[Android]',
                'iPhone': '[iOS]',
                'iPad': '[iOS]',
                'Mac': '[Mac OS]'
                }
    for each in mapping:
        if re.search(each, device):
            return mapping[each]
    return None

def infer_make_from_model(device):
    """Tries to map device models to manufacturer"""
    mapping = { 'Galaxy': '[Samsung]',
                'iPhone': '[Apple]',
                'iPad': '[Apple]',
                'Mac': '[Apple]'
                }
    for each in mapping:
        if re.search(each, device):
            return mapping[each]
    return None

def infer_make_from_os(device):
    """Tries to map device OS to manufacturer"""
    mapping = { 'iOS': '[Apple]',
                'Mac': '[Apple]'
                }
    for each in mapping:
        if re.search(each, device):
            return mapping[each]
    return None

def find_manufacturer(content):
    # these get more specific, the last match will be returned
    # this is so we scan for everything, can log hits and use for debugging

    manufacturers = ['Apple',
                     'Dell',
                     'Lenovo',
                     'Samsung',
                     'LG'
                     # more to do here...
                     ]
    return_value = None
    for each in manufacturers:
        res = re.search(each.lower(), content.lower())
        if res:
            return_value = each

    # returns the last value found
    return return_value


def find_software(content):
    """Returns a list of all the software mentioned in the supplied string"""

    software_list = ['Chrome',
                     'Firefox',
                     'Internet Explorer',
                     'Edge',
                     'Opera'

                     'Evernote',

                     'Telegram',
                     'WhatsApp',
                     'iMessage',
                     'Facetime',
                     'iCloud'
                     # TODO: more to do here...
                     ]
    return_value_list = []
    for each in software_list:
        res = re.search(each.lower(), content.lower())
        if res:
            return_value_list.append(each)

    return return_value_list
