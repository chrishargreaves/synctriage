import sys
import io
import csv
import logging
import operator
import core.merged_synced_device


class MergedDeviceList(object):

    def __init__(self):
        self.devices = []
        self.__current_pos = 0

    @property
    def named_devices(self):
        return [device for device in self.devices if device.name != '[Unknown]']

    @property
    def unnamed_devices(self):
        return [device for device in self.devices if device.name == '[Unknown]']

    def build_from_list(self, list_of_devices):
        """Populates the merged device list based on a supplied list"""
        if len(self.devices) > 0:
            raise ValueError('Building a merged list failed as one is already contained in this object.')
        for i, each_device in enumerate(sorted(list_of_devices, key=operator.attrgetter('no_name', 'name_lower', 'make', 'model', 'os'))):
            logging.info('Attempting to merge device {} ({}) into the device list...'.format(i, each_device.to_csv()))
            merge_result = self.merge_into_list_if_you_can(each_device)
            if merge_result:
                logging.info('Merge Success! Merged device is now {} [Total devices: {}]'.format(
                    merge_result.to_csv(), len(self.devices)))
            else:
                # else make and add new device
                logging.info('No suitable device found for merge. Creating new device...')
                self.add_new_from_sync_device(each_device)

            # only do this when really debugging merge process
            # logging.debug('='*40)
            # logging.debug(self.to_tsv())
            # logging.debug('=' * 40)

    def merge_into_list_if_you_can(self, new_device):
        """This attempts to merge the supplied device into the MergedDevice list"""
        for each_merged_device in self.devices:
            should_merge = each_merged_device.should_be_merged(new_device)
            if should_merge:
                logging.debug('Should attempt merge.')
                each_merged_device.add(new_device)
                return each_merged_device # once merged return the new merged device
        return None

    def add_new_from_sync_device(self, new_device):
        new_merge_device = core.merged_synced_device.MergedSyncedDevice()
        new_merge_device.add(new_device)
        self.devices.append(new_merge_device)

    def append(self, device):
        self.devices.append(device)

    def __len__(self):
        return len(self.devices)

    def __iter__(self):
        self.__current_pos = 0
        return self

    def __next__(self):
        if self.__current_pos >= len(self.devices):
            raise StopIteration
        else:
            self.__current_pos += 1
            return self.devices[self.__current_pos - 1]

    def check_device_in_list_by_name(self, test_device):
        """Checks if a device with the same name name is in the list already"""
        for i, each in enumerate(self.devices):
            if each.name == test_device.name:
                return i
        return None


    def to_tsv(self, subset=None, details=False):

        output = io.StringIO()
        writer = csv.writer(output, dialect='excel-tab')

        if subset == None:
            to_display = self.devices
        elif subset == 'named':
            to_display = self.named_devices
        elif subset == 'unnamed':
            to_display = self.unnamed_devices
        else:
            raise ValueError('Unsupported device subset ({})'.format(subset))

        if details:
            writer.writerow(('Name','Make','Model','OS','References','Software', 'Events', 'Info'))
        else:
            writer.writerow(('Name', 'Make', 'Model', 'OS', 'References'))

        for each in to_display:
            writer.writerow(each.to_list(details=details))
        return output.getvalue()

    def to_table(self, subset=None, details=False):
        import prettytable
        t = prettytable.PrettyTable(['Name','Make','Model','OS','Refs','Software', 'Events', 'Info'])
        t.align = 'l'
        t.border = True

        if subset == None:
            to_display = self.devices
        elif subset == 'named':
            to_display = self.named_devices
        elif subset == 'unnamed':
            to_display = self.unnamed_devices
        else:
            raise ValueError('Unsupported device subset ({})'.format(subset))

        for each in to_display:
            t.add_row(each.to_list(details=details))
        return str(t)

    def print_devices(self, filter=None, format=None):
        if filter == None:
            devices_to_print = self.devices
        elif filter == 'named':
            devices_to_print = self.named_devices
        elif filter == 'unnamed':
            devices_to_print = self.unnamed_devices
        else:
            raise ValueError('filter {} not supported'.format(filter))

        if format == None:
            print('=' * 20)
            for each in sorted(devices_to_print,
                           key=operator.attrgetter('name_lower', 'make', 'model', 'os')):
                print('=' * 20)
                print(each)
                print(each.get_log())
        elif format == 'tsv':
            print('=' * 20)
            print(self.to_tsv(subset=filter, details=True))
            print('=' * 20)
        elif format == 'table':
            print('=' * 20)
            print(self.to_table(subset=filter, details=True))
            print('=' * 20)

    def log_device_list(self, filter=None, title=''):
        """Write the merged device list to the log file"""
        if filter == None:
            devices_to_print = self.devices
        elif filter == 'named':
            devices_to_print = self.named_devices
        elif filter == 'unnamed':
            devices_to_print = self.unnamed_devices
        else:
            raise ValueError('filter {} not supported'.format(filter))

        out_str = '{}\n'.format(title)
        out_str += self.to_table(subset=filter, details=True)
        out_str += '\n'
        logging.info(out_str)


    def to_timeline(self):
        timeline = []
        for each_device in self.devices:
            for each_event in each_device.events:
                if each_device.name != '[Unknown]':
                    each_event.device = each_device.name
                elif each_device.model != '[Unknown]':
                    each_event.device = each_device.model
                elif each_device.os != '[Unknown]':
                    each_event.device = each_device.os
                else:
                    each_device.device = '[Unknown]'
                timeline.append(each_event)
        return sorted(timeline, key=operator.attrgetter('timestamp'))