import core.synced_device
import logging
import operator

class MergedSyncedDevice(core.synced_device.SyncedDevice):

    def __init__(self):
        super().__init__()
        self.base_synced_devices = []

    def __str__(self):
        out_str = 'DISCOVERED DEVICE\n'
        out_str += 'Name: {}\n'.format(self.name)
        out_str += 'Make: {}\n'.format(self.make)
        out_str += 'Model: {}\n'.format(self.model)
        out_str += 'OS: {}\n'.format(self.os)
        out_str += 'Software:\n'
        for each in sorted(self.installed_software):
            out_str += '\t{}\n'.format(each)
        out_str += 'Info:\n'
        for each in sorted(self.info):
            out_str += '\t{}:{}\n'.format(each, self.info[each])
        out_str += 'Events:\n'
        for each in sorted(self.events, key=operator.attrgetter('timestamp')):
            out_str += '\t{}\n'.format(each)
        out_str += 'Base Synced Devices: ({})\n'.format(len(self.base_synced_devices))
        for each in self.base_synced_devices:
            out_str += '\t{}\n'.format(each.to_table(show_source=True))

        return out_str

    def __get_best_case_from_strings(self, string1, string2):
        if string1.lower() != string2.lower():
            raise ValueError('Strings must have the same content')

        # title
        if string1.istitle():
            return string1
        if string2.istitle():
            return string2

        # detect mixed case
        if self.__ismixed(string1):
            return string1
        if self.__ismixed(string2):
            return string2

        # lower
        if string1.islower():
            return string1
        if string2.islower():
            return string2

        # upper
        if string1.isupper():
            return string1
        if string2.isupper():
            return string2
        else:
            # just return the first one
            return string1

    def __ismixed(self, the_string):
        """Checks if the supplied string has mixed case"""
        found_upper = False
        found_lower = False
        for each_chr in the_string:
            if each_chr.isupper():
                found_upper = True
            if each_chr.islower():
                found_lower = True
        if found_lower and found_upper:
            return True
        else:
            return False

    def should_be_merged(self, new_device):
        """Returns True if devices should be merged"""

        if self.name.lower() == new_device.name.lower() and self.name != '[Unknown]':
            logging.debug('Device \'{}\' should be merged with \'{}\' since name is the same.'.format(self.name,
                                                                                                      new_device.name))
            logging.debug('\tDevice 1: ' + self.to_tsv())
            logging.debug('\tDevice 2: ' + new_device.to_tsv())
            return True

        if self.model.lower().strip('[]') == new_device.model.lower().strip('[]') and self.model != '[Unknown]':
            if self.make == '[Unknown]' or new_device.make == '[Unknown]' or \
                            self.make.lower().strip('[]') == new_device.make.lower().strip('[]'):
                logging.debug('Device \'{}\' should be merged with \'{}\' since model is the same (\'{}\')'.format(self.name,
                                                                                                                   new_device.name,
                                                                                                                   self.model))
                logging.debug('\tDevice 1: ' + self.to_tsv())
                logging.debug('\tDevice 2: ' + new_device.to_tsv())
                return True
            else:
                logging.debug('Nearly merged based on model, but makes were different ({} != {})'.format(self.make, new_device.make))

        # if self.make.lower().strip('[]') == new_event.make.lower().strip('[]') and self.make != '[Unknown]':
        #     return True

        # logging.debug('Decided the following 2 devices were not merged:')
        # logging.debug(self.to_tsv())
        # logging.debug(new_device.to_tsv())
        return False


    def add(self, new_device):
        """Adds a device to the MergedDevice and integrates its information"""

        if len(self.base_synced_devices) != 0:
            logging.debug('Attempting to merge {} into {}'.format(new_device.name, self.name))
        else:
            pass # this is just a new device created and the details being copied over


        # Handle names
        if new_device.name == self.name:
            pass # good, they have the same name
        elif new_device.name.lower() == self.name.lower():
            # they are the same but in different cases?
            self.name = self.__get_best_case_from_strings(self.name, new_device.name)
        elif self.name == '[Unknown]' and new_device.name != '[Unknown]':
            self.name = new_device.name # this is good, we are getting more information
        elif self.name != '[Unknown]' and new_device.name == '[Unknown]':
            logging.debug('Ignored new [Unknown] name for devce named {}'.format(self.name))
            # don't overwrite a known name with unknown
        elif self.name != new_device.name:
            # neither are unknown, we have conflicting names, abort
            raise ValueError('Conflict merging device:\'{}\' with \'{}\''.format(new_device.name, self.name))

        # Handle make
        if new_device.make == self.make:
            pass  # good, they have the same make
        elif self.make == '[Unknown]' and new_device.make != '[Unknown]':
            self.make = new_device.make  # this is good, we are getting more information
        elif self.make != '[Unknown]' and new_device.make == '[Unknown]':
            logging.debug('Ignored new [Unknown] make for devce named {}'.format(self.name))
            # don't overwrite a known make with unknown
        elif new_device.make.strip('[]') == self.make:
            pass  # new is inferred, so keep current known
        elif new_device.make == self.make.strip('[]'):
            self.make = new_device.make # current is inferred, so keep new one that is known
        elif self.make != new_device.make:
            # neither are unknown, we have conflicting makes, abort
            raise ValueError('Conflict merging device:\'{}\' with \'{}\'. Different make: {}:{}'.format(
                new_device.name, self.name,
                new_device.make, self.make))

        # Handle model
        if new_device.model == self.model:
            pass  # good, they have the same model
        elif self.model == '[Unknown]' and new_device.model != '[Unknown]':
            self.model = new_device.model  # this is good, we are getting more information
        elif self.model != '[Unknown]' and new_device.model == '[Unknown]':
            logging.debug('Ignored new [Unknown] model for devce named {}'.format(self.name))
            # don't overwrite a known model with unknown
        elif new_device.model.strip('[]') == self.model:
            pass  # new is inferred, so keep current known
        elif new_device.model == self.model.strip('[]'):
            self.model = new_device.model # current is inferred, so keep new one that is known
        elif self.model != new_device.model:
            # neither are unknown, we have conflicting model, abort
            raise ValueError('Conflict merging device:\'{}\' with \'{}\'. Different model: {}:{}'.format(
                new_device.name, self.name,
                new_device.model, self.model))

        # Handle os
        if new_device.os == self.os:
            pass  # good, they have the same os
        elif self.os == '[Unknown]' and new_device.os != '[Unknown]':
            self.os = new_device.os  # this is good, we are getting more information
        elif self.os != '[Unknown]' and new_device.os == '[Unknown]':
            logging.debug('Ignored new [Unknown] OS for devce named {}'.format(self.name))
            # don't overwrite a known os with unknown
        elif self.os_major_version == new_device.os_major_version:
            # merge but keep longest - idea is that the longest is more specific
            longest = max([self.os, new_device.os], key=len)
            self.os = longest
        elif self.os != new_device.os:
            # neither are unknown, we have conflicting os, abort
            raise ValueError('Conflict merging device:\'{}\' with \'{}\'. Different OS: \'{}\' vs \'{}\''.format(
                new_device.name, self.name,
                new_device.os, self.os))


        self.installed_software.extend(x for x in new_device.installed_software if x not in self.installed_software)
        self.events.extend(x for x in new_device.events if x not in self.events)

        # copy info but preserve multiples
        for each_info in new_device.info:
            self.add_info(each_info, new_device.info[each_info])

        self.base_synced_devices.append(new_device)

        return True     # all succesful, returns true

    def to_list(self, details=False):
        if details:
            return (self.name, self.make, self.model, self.os,
                    len(self.base_synced_devices), len(self.installed_software),
                    len(self.events), len(self.info))
        else:
            return (self.name, self.make, self.model, self.os,
                    len(self.base_synced_devices))

    def to_tsv(self, details=False):
        if details:
            out_str = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(self.name, self.make, self.model, self.os,
                                                            len(self.base_synced_devices), len(self.installed_software),
                                                            len(self.events), len(self.info))
        else:
            out_str = '{}\t{}\t{}\t{}\t{}'.format(self.name, self.make, self.model, self.os, len(self.base_synced_devices))
        return out_str


    def to_csv(self):
        out_str = '"{}", "{}", "{}", "{}", "{}"'.format(self.name, self.make, self.model, self.os, len(self.base_synced_devices))
        return out_str


