import dfpy.types.device
import core.device_entity_finder
import inspect
import re
import operator
import logging


class SyncedDevice(dfpy.types.device.Device):

    def __init__(self):
        self.name = '[Unknown]'
        self.make = '[Unknown]'
        self.model = '[Unknown]'
        self.os = '[Unknown]'
        self.installed_software = []
        self.events = []
        self.info = {}
        self.source_file = ''
        self.__log = ''
        creating_script = '[not implemented]'
        self.log('SyncedDevice object created by \'{}\''.format(creating_script))

        # creating_script = inspect.stack()[1].filename # not on Python3.4


    @property
    def no_name(self): # this is used to be able to put the named devices at the top of a list
        if self.name == '[Unknown]':
            return True
        else:
            return False

    @property
    def name_lower(self):
        """Property for lowercase version of the device name, useful for sorting"""
        return self.name.lower()

    @property
    def os_major_version(self):
        major_os = core.device_entity_finder.get_major_os_from_string(self.os)
        if major_os:
            return major_os
        else:
            return self.os

    def add_info(self, key, value):
        if key not in self.info:
            self.info[key] = value
            return
        else:
            if self.info[key] == value: # if same, then dont add
                return

            for i in range(2,500):
                if self.info.get(key + '[{}]'.format(i-1)) == value:
                    return # quit without adding, don't need duplicates
                new_key = key + '[{}]'.format(i)
                if new_key not in self.info:
                    self.info[new_key] = value
                    return
        logging.error('Did not add key {}:{} to device. Max keys reached'.format(key, value))

    def __str__(self):
        out_str = 'DEVICE\n'
        out_str += 'Name: {}\n'.format(self.name)
        out_str += 'Make: {}\n'.format(self.make)
        out_str += 'Model: {}\n'.format(self.model)
        out_str += 'OS: {}\n'.format(self.os)
        out_str += 'Software:\n'
        for each in sorted(self.installed_software):
            out_str += '\t{}\n'.format(each)
        out_str += 'Info:\n'
        for each in sorted(self.info):
            out_str += '\t{}:{}\n'.format(each, self.info[each])
        out_str += 'Events:\n'
        for each in sorted(self.events, key=operator.attrgetter('timestamp')):
            out_str += '\t{}\n'.format(each)
        return out_str

    def to_csv(self):
        out_str = '"{}", "{}", "{}", "{}"'.format(self.name, self.make, self.model, self.os)
        return out_str

    def to_tsv(self, show_source=False):
        if show_source:
            out_str = '"{}"\t"{}"\t"{}"\t"{}"\t"{}"'.format(self.name, self.make, self.model, self.os, self.source_file)
        else:
            out_str = '"{}"\t"{}"\t"{}"\t"{}"'.format(self.name, self.make, self.model, self.os)
        return out_str

    def to_table(self, show_source=False):
        if show_source:
            out_str = '{0:20} {1:10} {2:10} {3:10} {4:10}'.format(self.name, self.make, self.model, self.os, self.source_file)
        else:
            out_str = '{0:20} {1:10} {2:10} {3:10}'.format(self.name, self.make, self.model, self.os)
        return out_str


    def to_list(self, details=False):
        if details:
            return (self.name, self.make, self.model, self.os, self.source_file)
        else:
            return (self.name, self.make, self.model, self.os)


    def log(self, log_entry):
        self.__log += log_entry + '\n'

    def get_log(self):
        return 'LOG:\n' + self.__log

    def isblank(self):
        """Determines if the device has any information associated with it or not"""
        if self.name != '[Unknown]':
            return False
        if self.make != '[Unknown]':
            return False
        if self.model != '[Unknown]':
            return False
        if self.os != '[Unknown]':
            return False
        # for each in self.info:
        #     if self.info[each] != None:
        #         return False
        # for each in self.installed_software:
        #     if each != None:
        #         return False
        # for each in self.events:
        #     if each != None:
        #         return False
        return True

    def ingest_data_from_string(self, string_with_values):
        """If string passed contains any pertinent information then absorb it into the device object"""
        model = core.device_entity_finder.find_device(string_with_values)
        if model:
            self.model = model

        make = core.device_entity_finder.find_manufacturer(string_with_values)
        if make:
            self.make = make

        sw = core.device_entity_finder.find_software(string_with_values)
        if sw:
            self.installed_software.extend(sw)

        the_os = core.device_entity_finder.find_os(string_with_values)
        if the_os:
            self.os = the_os


    def add_event(self, the_date, the_location, the_event, the_source, utc=False):
        if the_date is None:
            raise ValueError('Date cannot be None')
        if the_date == 'None':
            raise ValueError('Date cannot be str \'None\'')
        if not isinstance(the_date, str) or len(the_date) < 18:
            raise ValueError('Date {} ({}) is not correctly formatted or wrong type'.format(the_date, type(the_date)))
        if the_date[4] == ':' or the_date[7] == ':':
            the_date = '{}-{}-{} {}'.format(the_date[0:4], the_date[5:7], the_date[8:10], the_date[11:])

        if utc:
            the_date += '*'

        a = SynctriageDeviceEvent(the_date, the_location, the_event, the_source)
        self.events.append(a)


class SynctriageDeviceEvent(object):

    def __init__(self, the_timestamp, the_location, the_event, the_source):
        self.timestamp = the_timestamp
        self.location = the_location
        self.event = the_event
        self.source = the_source
        self.device = ''

    def __str__(self):
        out_str = '{0:23} {1:30} {2}'.format(self.timestamp, str(self.location), self.event)
        return out_str