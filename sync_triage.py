import logging
import argparse
import operator
import os
import sys
import time
import prettytable
import core.synctriage_logging
import core.merged_synced_device_list
import core.merged_synced_device
import core.device_entity_finder
import core.device_list_processing
import core.research_mode
import sync_triage_win
import sync_triage_ios
import sync_triage_android
import dfpy.utils.os_detection
import dfpy.utils.users


def main():
    start_time = time.time()
    # Parse all the arguments passed to the program
    parser = argparse.ArgumentParser(descriptioen='Perform triage based on syncronisation artefacts')
    parser.add_argument("--details", help="prints details of discovered devices rather than a summary", action="store_true")
    parser.add_argument("--debug", help="adds detail to log file", action="store_true")
    parser.add_argument("--timeline", help="prints a basic timeline of activity on the discovered devices", action="store_true")
    parser.add_argument("--research", help="(experimental!) on completion program searches for known device names in other files", action="store_true")
    parser.add_argument('path', type=str,
                        help='path to target device. this can be the root of a mounted disk image, an iOS backup folder, or an Android .ab backup file.')
    args = parser.parse_args()

    if args.debug:
        print('Debug mode on')

    # Enable Logging
    if not core.synctriage_logging.enable_logging(args.debug):
        print('Unable to start logging. Exiting...')
        sys.exit(-1)

    if not os.path.exists(args.path):
        logging.error('Path {} not found'.format(args.path))

    logging.info('Detecting OS...')
    target_os = dfpy.utils.os_detection.detect_os_in_target_path(args.path)
    print('Detected OS: {}'.format(target_os))
    logging.info('Detected OS: {}'.format(target_os))

    list_of_devices = []
    # run correct plugins for os
    if target_os == 'vista+':
        print('Running Windows plugins...')
        list_of_devices = sync_triage_win.process_mounted_windows_disk_image(args.path)
    elif target_os == 'ios_backup':
        print('Running iOS backup plugins...')
        list_of_devices = sync_triage_ios.process_ios_backup(args.path)
    elif target_os == 'android_backup':
        print('Running Android backup plugins...')
        list_of_devices = sync_triage_android.process_android_backup(args.path)
    else:
        print('No supported OS detected')
        sys.exit(-1)

    logging.info('Device detection completed.')
    print('Device detection completed.')
    logging.info('Total potential devices detected: {}'.format(len(list_of_devices)))
    print('Total potential devices detected: {}'.format(len(list_of_devices)))

    if args.debug:
        print(pretty_print_device_list(list_of_devices)) # initial list from detection
        pretty_log_device_list(list_of_devices, 'LIST OF DEVICES FROM PLUGINS')

    list_of_devices = core.device_list_processing.preprocess_device_list(list_of_devices)

    logging.debug('UPDATED LIST:')
    pretty_log_device_list(list_of_devices, 'LIST OF DEVICES AFTER PRE-PROCESSING')  # device list post processing

    merged_device_list = core.device_list_processing.merge_devices(list_of_devices)
    merged_device_list.log_device_list('named', 'NAMED DEVICE LIST (MERGED)')
    merged_device_list.log_device_list('unnamed', 'UNNAMED DEVICE LIST (MERGED)')

    print('='*20)
    if args.details:
        print('NAMED DEVICE LIST DETAILS ({}) (MERGED) :'.format(len(merged_device_list.named_devices)))
        merged_device_list.print_devices(filter='named')

        print('UNNAMED DEVICE LIST DETAILS ({}) (MERGED) :'.format(len(merged_device_list.unnamed_devices)))
        merged_device_list.print_devices(filter='unnamed')
    else:
        print('NAMED DEVICE LIST ({}) (MERGED)'.format(len(merged_device_list.named_devices)))
        merged_device_list.print_devices(filter='named', format='table')

        print('UNNAMED DEVICE LIST ({}) (MERGED)'.format(len(merged_device_list.unnamed_devices)))
        merged_device_list.print_devices(filter='unnamed', format='table')

    # also put it in r&D mode where all the device names that are foudn are searched for on the whole disk
    if args.research:
        print('Entered Research mode... (this might take a while)...')
        name_list = [x.name for x in merged_device_list.named_devices]
        research_results = core.research_mode.find_relevant_files(args.path, name_list)
        print('--- RESEARCH MODE RESULTS ---')
        for each_research_result in sorted(research_results):
            print(each_research_result)
        print('-----------------------------\n')

    logging.info('SyncTriage program complete')
    logging.info('Summary Info')
    logging.info('============')
    logging.info('Potential devices: {}'.format(len(list_of_devices)))
    logging.info('Merged devices: {}'.format(len(merged_device_list)))
    logging.info('-> Named devices: {}'.format(len(merged_device_list.named_devices)))
    for each in merged_device_list.named_devices:
        logging.info('\t -> {}'.format(each.to_tsv()))
    logging.info('-> Unnamed devices: {}'.format(len(merged_device_list.unnamed_devices)))
    # for each in merged_device_list.unnamed_devices:
    #     logging.info('\t -> {}'.format(each.to_tsv()))
    logging.info('Date range covered: x - y')

    if args.timeline:
        print('UNIVERSAL TIMELINE')
        print('==================')
        timeline = merged_device_list.to_timeline()
        for each in timeline:
            try:
                print('{0:100}\t{1:15}\t{2}'.format(str(each), str(each.device), str(each.source)))
            except UnicodeEncodeError:
                print('error with {}'.format(each.source))

    end_time = time.time()
    print("\nProgram complete in {} seconds".format(round(end_time - start_time, 4)))
    print('='*30)
    logging.info("Program complete in {} seconds".format(round(end_time - start_time, 4)))
    sys.exit(0)


def print_device_list(list_of_devices):
    print('-'*20)
    for each in sorted(list_of_devices, key=operator.attrgetter('no_name', 'name_lower', 'make', 'model', 'os')):
        print(each.to_tsv(show_source=True))
    print('-'*20)


def pretty_print_device_list(list_of_devices):
    out_str = ''
    out_str += '-' * 20 + '\n'
    t = prettytable.PrettyTable(['Name', 'Make', 'Model', 'OS', 'Source'])
    t.align = 'l'
    t.border = False
    for each in sorted(list_of_devices, key=operator.attrgetter('no_name', 'name_lower', 'make', 'model', 'os')):
        t.add_row(each.to_list(details=True))
    out_str += str(t) + '\n'
    out_str += '-' * 20 + '\n'
    return out_str


def pretty_log_device_list(list_of_devices, title=''):
    devices = pretty_print_device_list(list_of_devices)
    logging.debug('{}\n{}'.format(title, devices))


def log_device_list(list_of_devices):
    logging.debug('-'*20)
    for each in sorted(list_of_devices, key=operator.attrgetter('no_name', 'name_lower', 'make', 'model', 'os')):
        logging.info(each.to_tsv(show_source=True))
    logging.debug('-'*20)


if __name__ == '__main__':
    main()

