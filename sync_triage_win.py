import logging
import dfpy.utils.users

import extractors.windows.icloud_photos
import extractors.windows.dropbox_photos
import extractors.generic.chrome
import extractors.windows.email_scanner
import extractors.windows.evernote
import extractors.windows.viber

def process_mounted_windows_disk_image(path_to_root_dir):
    # need to get user list and process one at a time
    logging.info('Using Windows Plugins:')
    logging.info('----------------------')

    logging.info('Locating user home folders...')
    user_home_folder_list = dfpy.utils.users.get_list_of_home_folders(path_to_root_dir, filter_common=True)
    logging.info('Found {} users:'.format(len(user_home_folder_list)))
    logging.info('-> {}'.format(str(user_home_folder_list)))
    list_of_devices = []
    for each_user_home_path in user_home_folder_list:
        logging.info('-'*20)
        logging.info('Processing home folder: {}...'.format(each_user_home_path))

        logging.info('*'*20)
        logging.info('Running iCloud plugin...')
        identified_devices = extractors.windows.icloud_photos.get_devices_from_icloud_paths(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*'*20)
        logging.info('Running iCloud Database plugin...')
        identified_devices = extractors.windows.icloud_photos.get_device_info_from_databases(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*' * 20)
        logging.info('Running Dropbox plugin...')
        identified_devices = extractors.windows.dropbox_photos.get_devices_from_dropbox_paths(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*' * 20)
        logging.info('Running Chrome plugin...')
        identified_devices = extractors.generic.chrome.get_devices_from_session_sync_db(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*' * 20)
        logging.info('Running Windows mail plugin...')
        identified_devices = extractors.windows.email_scanner.get_devices_from_windows_emails(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*' * 20)
        logging.info('Running Evernote plugin...')
        identified_devices = extractors.windows.evernote.get_devices_from_evernote(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

        logging.info('*' * 20)
        logging.info('Running Viber plugin...')
        identified_devices = extractors.windows.viber.get_devices_from_viber(each_user_home_path)
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        logging.info('Discovered {} devices'.format(len(identified_devices)))
        list_of_devices.extend(identified_devices)
        logging.info('*' * 20)

    return list_of_devices


if __name__ == '__main__':
    pass