from distutils.core import setup
import py2exe
import sys

print(sys.path)

setup(name='sync_triage',
      version='0.11',
      description='A tool for triage based on syncronisation artefacts',
      author='Chris Hargreaves',
      author_email='chris@hargs.co.uk',
      py_modules=['core.device_entity_finder',
                  'core.merged_synced_device',
                  'core.merged_synced_device_list',
                  'core.synced_device',
                  'core.synctriage_logging',
                  'extractors.android.googlephotos',
                  'extractors.generic.chrome',
                  'extractors.generic.email_scanner',
                  'extractors.generic.exif_tool',
                  'extractors.ios.chrome',
                  'extractors.ios.evernote',
                  'extractors.ios.firefox',
                  'extractors.ios.sms',
                  'extractors.windows.dropbox_photos',
                  'extractors.windows.email_scanner',
                  'extractors.windows.evernote',
                  'extractors.windows.firefox',
                  'extractors.windows.icloud_photos',
                  'sync_triage',
                  'sync_triage_win',
                  'sync_triage_android',
                  'sync_triage_ios',
                  'sync_triage_mac'
                  ],
      console=['sync_triage.py'],
      options = {
        'py2exe': { 'bundle_files': 1,
                    'compressed': 2,
                    'includes': [],
                    'packages': ['dfpy']
                },
      },
      zipfile=None
)
