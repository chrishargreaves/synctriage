import os
import logging
import pymobilesupport.iphone_backup
import extractors.ios.sms
import extractors.ios.firefox
import extractors.ios.chrome


def process_ios_backup(path_to_backup_folder):

    logging.info('Using iOS Plugins:')
    logging.info('----------------------')

    ios_backup = pymobilesupport.iphone_backup.iOSBackup(path_to_backup_folder)

    list_of_devices = []

    # list all files in backup
    file_list = ios_backup.get_file_list()
    for each in sorted(file_list):
        logging.debug(each)

    # Process SMS database
    identified_devices = extractors.ios.sms.process_sms_in_backup_folder(ios_backup)
    list_of_devices.extend(identified_devices)
    logging.info('Added {} device from SMS'.format(len(identified_devices)))

    # Firefox
    identified_devices = extractors.ios.firefox.process_firefox_browserdb_in_backup_folder(ios_backup)
    list_of_devices.extend(identified_devices)
    logging.info('Added {} device from Firefox'.format(len(identified_devices)))

    # Chrome
    identified_devices = extractors.ios.chrome.get_devices_from_ios_chrome_syncdb(ios_backup)
    list_of_devices.extend(identified_devices)
    logging.info('Added {} device from Chrome'.format(len(identified_devices)))


    print('Found {} devices'.format(len(list_of_devices)))

    if os.path.exists(os.path.join(path_to_backup_folder, 'Filesystem')):
        print('There is also a \'Filessytem\' folder to process')


    return list_of_devices
