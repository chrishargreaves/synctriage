# README #

SyncTriage is designed to process a disk image of a device and identify the presence of devices and  information about them.

Based on work in:

Hargreaves, C. Marshall, A., SyncTriage: Using synchronisation artefacts to optimise acquisition order,
Digital Investigation, Volume 28, Supplement, 2019, Pages S134-S140, ISSN 1742-2876, https://doi.org/10.1016/j.diin.2019.01.022.

Presented at DFRWS EU 2018
https://dfrws.org/presentation/synctriage-using-synchronisation-artefacts-to-optimize-acquisition-order/ 


### Installation ###

#### Windows ####

* Download the source and install the dependencies in requirements.txt

#### Linux & Mac OS #####

* Download the source and install the dependencies in requirements.txt

### Usage ###

The tool can be run against a mounted disk image, iOS backup folder, or Android backup .ab file. 

The general usage is:


```
#!bash
usage: sync_triage.exe [-h] [--details] [--debug] [--timeline] path

Perform triage based on syncronisation artefacts
`positional arguments:
  path        path to target device. this can be the root of a mounted disk
              image, an iOS backup folder, or an Android .ab backup file.

optional arguments:
  -h, --help  show this help message and exit
  --details   prints details of discovered devices rather than a summary
  --debug     adds detail to log file
  --timeline  prints a basic timeline of activity on the discovered devices`

```

#### Disk Image Example ####

* Mount your disk image (FTK Imager has been tested). The following settings have been tested to work:

![ftk.png](https://bitbucket.org/repo/468zoR/images/3076301274-ftk.png)

* You will now have one or more extra drive letters and for each drive you should find a subfolder called '[root]'. Find the one that contains your Windows partition. 

* run: `sync_triage E:\[root] ` (where E:\ is the drive letter with your Windows partition. 

That's it.

#### iOS Backup Example ####

TODO

#### Android Backup Example ####

TODO

#### Example Output ####

TODO
